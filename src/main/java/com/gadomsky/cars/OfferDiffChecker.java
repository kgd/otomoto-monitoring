package com.gadomsky.cars;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class OfferDiffChecker {

    static List<OfferEvent> diff(List<Offer> prev, List<Offer> current) {
        Set<OfferId> currentIds = current.stream().map(Offer::id).collect(Collectors.toSet());
        Set<OfferId> oldIds = prev.stream().map(Offer::id).collect(Collectors.toSet());

        var deletedOffers = prev.stream().filter(o -> !currentIds.contains(o.id())).map(OfferEvent::deleted).toList();
        var addedOffers = current.stream().filter(f -> !oldIds.contains(f.id())).map(OfferEvent::added).toList();


        var prevMap = prev.stream().collect(Collectors.toMap(Offer::id, o -> o, (a, b) -> a));
        var currMap = current.stream().collect(Collectors.toMap(Offer::id, o -> o, (a, b) -> a));

        var changedIds = current.stream()
                .filter(c -> !addedOffers.stream().map(a -> a.id).toList().contains(c.id().id()))
                .filter(c -> !prevMap.get(c.id()).values().equals(c.values()))
                .map(Offer::id)
                .toList();

        var changedOffers = changedIds.stream().map(id -> OfferEvent.changed(prevMap.get(id), currMap.get(id))).flatMap(Collection::stream).toList();

        return Stream.of(deletedOffers, addedOffers, changedOffers).flatMap(Collection::stream).toList();
    }
}
