package com.gadomsky.cars;

import com.gadomsky.cars.csv.CsvReader;
import com.gadomsky.cars.csv.CsvWriter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class OfferMonitoringApp {

    String type = System.getenv("CAR");
    String snapshotFile = createdFilePath("data_" + type + ".csv");
    String eventsFile = createdFilePath("events_" + type + ".csv");
    private CsvReader<OfferSnapshot> snapshotReader = new CsvReader<>(OfferSnapshot.class);
    private CsvWriter<OfferSnapshot> snapshotWriter = new CsvWriter<>(OfferSnapshot.class);
    private CsvReader<OfferEvent> eventReader = new CsvReader<>(OfferEvent.class);
    private CsvWriter<OfferEvent> eventWriter = new CsvWriter<>(OfferEvent.class);

    private String otomoto = System.getenv(type + "_URL");
    private Fetcher fetcher = new MultiPageFetcher(otomoto);
    private Notifier notifier = Notifier.defaultNotifier();

    void run() {
        System.out.println("Checking " + type);
        System.out.println("Url: " + otomoto);
        List<OfferSnapshot> prevOffersSnapshots = snapshotReader.read(snapshotFile);
        var offers = prevOffersSnapshots.stream().map(OfferSnapshot::toDomain).toList();
        var newOffers = fetcher.fetch().offers();

        var prevEvents = eventReader.read(eventsFile);
        var newEvents = OfferDiffChecker.diff(offers, newOffers);
        eventWriter.write(eventsFile, Stream.of(prevEvents, newEvents).flatMap(Collection::stream).toList());
        snapshotWriter.write(snapshotFile, newOffers.stream().map(Offer::toSnapshot).sorted(Comparator.comparing(o -> o.id)).toList());

        if (newEvents.size() > 0) {
            notifier.notify(newEvents);
        }
    }

    private String createdFilePath(String path) {
        var f = new File(path);
        if (!f.exists()) {
            try {
                Files.createFile(f.toPath());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return path;
    }

    public static void main(String[] args) {
        new OfferMonitoringApp().run();
    }
}

