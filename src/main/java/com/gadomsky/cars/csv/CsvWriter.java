package com.gadomsky.cars.csv;

import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CsvWriter<T> {
    final Class<? extends T> type;

    public CsvWriter(Class<? extends T> type) {
        this.type = type;
    }

    public void write(String fileName, List<T> data) {
        var mappingStrategy = new HeaderColumnNameMappingStrategy(true);
        mappingStrategy.setType(type);
        FileWriter writer = getWriter(fileName);
        try {
            new StatefulBeanToCsvBuilder<T>(writer)
                    .withMappingStrategy(mappingStrategy)
                    .build()
                    .write(data);
            writer.flush();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    private FileWriter getWriter(String fileName) {
        try {
            return new FileWriter(fileName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

