package com.gadomsky.cars.csv;

import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class CsvReader<T> {
    final Class<? extends T> type;

    public CsvReader(Class<? extends T> type) {
        this.type = type;
    }

    public List<T> read(String fileName) {
        var mappingStrategy = new HeaderColumnNameMappingStrategy(true);
        mappingStrategy.setType(type);

        return new CsvToBeanBuilder(getReader(fileName))
                .withType(type)
                .withMappingStrategy(mappingStrategy)
                .build()
                .parse();
    }

    private FileReader getReader(String fileName) {
        try {
            return new FileReader(fileName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
