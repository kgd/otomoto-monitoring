package com.gadomsky.cars;

import java.util.Arrays;

public class OfferSnapshot {
    String id;
    String url;
    String params;
    String title;

    String titleExtended;
    String price;
    String location;

    public OfferSnapshot() {
    }

    public OfferSnapshot(String id, String url, String params, String title,  String titleExtended, String price, String location) {
        this.id = id;
        this.url = url;
        this.params = params;
        this.title = title;
        this.titleExtended = titleExtended;
        this.price = price;
        this.location = location;
    }

    Offer toDomain() {
        return new Offer(new OfferId(id), title, titleExtended, price, url, location, Arrays.asList(params.split("\\|")));
    }

}


